﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
    public class NewsPartial
    {
        [MetadataType(typeof(NewsMetadata))]
        public partial class News
        {
        }

        public class NewsMetadata
        {
            [DispalyName   ("標題")]
            [Required]
            [StringLength(50)]
            public int Title { get; set; }

            [DispalyName("內容")]
            [Required]
            [StringLength(500)]
            public string Content { get; set; }
            
            [DispalyName("建立時間")]
            [Required]
            public System.DateTime CreatTime { get; set; }
        } 

    }
}